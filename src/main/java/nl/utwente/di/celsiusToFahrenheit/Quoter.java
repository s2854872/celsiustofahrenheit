package nl.utwente.di.celsiusToFahrenheit;

public class Quoter {

    public double getFahrenHeit(String celsius) {
        return (Double.parseDouble(celsius) * 9/5) + 32;
    }
}
